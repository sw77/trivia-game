//
//  Answer.swift
//  TriviaGame
//
//  Created by Ahmed Adel on 5/9/22.
//

import Foundation

struct Answer:Identifiable{
    var id = UUID()
    var text: AttributedString
    var isCorrect: Bool
}
