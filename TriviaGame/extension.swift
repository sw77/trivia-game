//
//  extension.swift
//  TriviaGame
//
//  Created by Ahmed Adel on 5/9/22.
//

import Foundation
import SwiftUI

extension Text{
    func lilacTitle() -> some View{
        self.font(.title)
            .fontWeight(.heavy)
            .foregroundColor(Color("AccentColor"))
    }
}
