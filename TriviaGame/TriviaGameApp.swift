//
//  TriviaGameApp.swift
//  TriviaGame
//
//  Created by Ahmed Adel on 5/9/22.
//

import SwiftUI

@main
struct TriviaGameApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
